LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),m1892)

$(call add-radio-file,firmware/abl.elf)
$(call add-radio-file,firmware/tz.mbn)
$(call add-radio-file,firmware/xbl.elf)
$(call add-radio-file,firmware/qupv3fw.elf)
$(call add-radio-file,firmware/splash.mbn)
$(call add-radio-file,firmware/xbl_config.elf)
$(call add-radio-file,firmware/BTFM.bin)
$(call add-radio-file,firmware/keymaster64.mbn)
$(call add-radio-file,firmware/hyp.mbn)
$(call add-radio-file,firmware/storsec.mbn)
$(call add-radio-file,firmware/devcfg.mbn)
$(call add-radio-file,firmware/NON-HLOS.bin)
$(call add-radio-file,firmware/cmnlib.mbn)
$(call add-radio-file,firmware/cmnlib64.mbn)
$(call add-radio-file,firmware/dspso.bin)
$(call add-radio-file,firmware/aop.mbn)

ifeq ($(PLATFORM_VERSION), 11)
$(call add-radio-file,firmware/vbmeta.img)
endif

endif
